export default {
  init() {
     const WebFont = require('webfontloader');

     WebFont.load({
      google: {
        families: ['Muli:400,900&display=swap'],
      },
    });
  },
}
