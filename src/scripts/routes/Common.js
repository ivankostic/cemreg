// JS components
import TypeFont from '../components/TypeFont';
//import '../vendors/jquery-modal-video';

export default {
  init() { // scripts here run on the DOM load event
    TypeFont.init();
  },
  finalize() { // scripts here fire after init() runs
    $(".js-modal-btn").modalVideo({channel:'vimeo', autoplay:true, autopause:false});
  },
};
